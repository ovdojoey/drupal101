<?php

/* themes/custom/jdrupal/templates/views-view.html.twig */
class __TwigTemplate_0a3d5eca210ac8a738ce81ace46dde8a87b65e1b0330503ddc88607190d92d8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["rows"]) ? $context["rows"] : null)) {
            // line 2
            echo "  ";
            echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["rows"]) ? $context["rows"] : null), "html", null, true);
            echo "
";
        } elseif (        // line 3
(isset($context["empty"]) ? $context["empty"] : null)) {
            // line 4
            echo "<div class=\"view-empty\">
  ";
            // line 5
            echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["empty"]) ? $context["empty"] : null), "html", null, true);
            echo "
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/jdrupal/templates/views-view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 5,  28 => 4,  26 => 3,  21 => 2,  19 => 1,);
    }
}
