<?php

/* themes/custom/jdrupal/templates/node.html.twig */
class __TwigTemplate_b7d1ce490a2c5534794a5c00beec22359d346238dad034e6b68856059ac48400 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<figure class=\"step\">
  <div class=\"step__count activate_step\" data-activate=\"";
        // line 2
        echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_step", array()), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_step", array()), "html", null, true);
        echo "</div>
  <div class=\"step__title\">";
        // line 3
        echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
        echo "</div>
  <div class=\"step__description\">
    ";
        // line 5
        echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "body", array()), "html", null, true);
        echo "

    <div class=\"step__exit deactivate_step\">
      <a class=\"deactivate_step_bar\"></a>
    </div>
  
   <div class=\"activate_next_step\" data-activate=\"";
        // line 11
        echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_step", array()), "html", null, true);
        echo "\">
    <div class=\"next_container\">
      <a class=\"activate_step_bar\"></a>
     </div>
    </div>

</figure>


";
    }

    public function getTemplateName()
    {
        return "themes/custom/jdrupal/templates/node.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 11,  33 => 5,  28 => 3,  22 => 2,  19 => 1,);
    }
}
