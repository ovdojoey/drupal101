<?php

/* themes/custom/jdrupal/templates/container.html.twig */
class __TwigTemplate_ca964a9ef641b9ea8bf3621c995568299167f9cdd4ab72e0c84f51d2b61f0c9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["children"]) ? $context["children"] : null), "html", null, true);
    }

    public function getTemplateName()
    {
        return "themes/custom/jdrupal/templates/container.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
