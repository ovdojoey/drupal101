<?php

/* themes/custom/jdrupal/templates/field--node--field-step.html.twig */
class __TwigTemplate_3fc560568be4de4b376c340abfdff48dbeab0d28920fbc679d76281f37c12706 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["items"]) ? $context["items"] : null), "html", null, true);
    }

    public function getTemplateName()
    {
        return "themes/custom/jdrupal/templates/field--node--field-step.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
