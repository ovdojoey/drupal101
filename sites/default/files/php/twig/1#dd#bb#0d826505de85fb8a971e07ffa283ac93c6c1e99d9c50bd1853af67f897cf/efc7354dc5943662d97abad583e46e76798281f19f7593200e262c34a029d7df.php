<?php

/* themes/custom/jdrupal/templates/block.html.twig */
class __TwigTemplate_ddbb0d826505de85fb8a971e07ffa283ac93c6c1e99d9c50bd1853af67f897cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header\">
\t<h1 class=\"title\">Drupal 101</h1>
</header>
<noscript>
\t<div class=\"noscript\">
\t\tThis site uses JavaScript.  Please enable JavaScript or upgrade your browser to view this website.
\t</div>
</noscript>
<div class=\"step-container\" id=\"j-step-container\">
\t<div class=\"opener-text\">
\t\t<span class=\"eight\">8</span>
\t\t<span class=\"getting-started\">Getting Started</span>
\t\t<span class=\"tips\">Tips</span>
\t</div>
\t";
        // line 15
        echo $this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["content"]) ? $context["content"] : null), "html", null, true);
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/jdrupal/templates/block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 15,  19 => 1,);
    }
}
