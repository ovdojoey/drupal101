(function () {

  window.onload = function () {

    var container = document.getElementById('j-step-container'),
      activateStep = Array.prototype.slice.call(document.getElementsByClassName('activate_step')),
      deactivateStep = Array.prototype.slice.call(document.getElementsByClassName('deactivate_step'));

    function changeStep() {
      container.className = 'step-container activate activate-' + this.dataset.activate;
    }

    function addChangeStepOnClick(elem) {
      elem.addEventListener('click', changeStep, false);
    }


    function removeStep() {
      container.className = 'step-container activate deactivate';
    }

    function deactivateStepOnClick(elem) {
      elem.addEventListener('click', removeStep, false);
    }

    function init() {
      activateStep.forEach(addChangeStepOnClick);
      deactivateStep.forEach(deactivateStepOnClick);
    }

    setTimeout(function () { container.className = 'step-container activate'; }, 50);
    init();

  };

}());;
