(function () {

  window.onload = function () {

    var container = document.getElementById('j-step-container'),
      activateStep = Array.prototype.slice.call(document.getElementsByClassName('activate_step')),
      activateNextStep = Array.prototype.slice.call(document.getElementsByClassName('activate_next_step')),
      deactivateStep = Array.prototype.slice.call(document.getElementsByClassName('deactivate_step'));

    function changeStep() {
      container.className = 'step-container activate activate-' + this.dataset.activate;
    }

    function addChangeStepOnClick(elem) {
      elem.addEventListener('click', changeStep, false);
    }

    function addChangeNextStepOnClick(elem) {
      var _nextStep = elem.dataset.activate;
      _nextStep++;

      if (_nextStep > 8) {
        _nextStep = 1;
      }

      elem.addEventListener('click', function () { container.className = 'step-container activate activate-' + _nextStep; }, false);
    }

    function removeStep() {
      container.className = 'step-container activate deactivate';
    }

    function deactivateStepOnClick(elem) {
      elem.addEventListener('click', removeStep, false);
    }

    function init() {
      activateStep.forEach(addChangeStepOnClick);
      activateNextStep.forEach(addChangeNextStepOnClick);
      deactivateStep.forEach(deactivateStepOnClick);
    }

    setTimeout(function () { container.className = 'step-container activate'; }, 50);
    init();

  };

}());